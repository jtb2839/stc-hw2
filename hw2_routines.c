//James Bell
#include "hw2_routines.h"
#include "math.h"
double trapezoid(int lower, int upper, int divs)
{
	double length = upper-lower;
	double dl = length/divs;
	double sum = pow(lower,7)+pow(upper,7);
	double i;
	for(i=lower;i<upper;i=i+dl)
	{
		sum=sum + 2*pow(i,7);
	}
	sum=sum*dl/2;
	return sum;
}
double simpsons(int lower, int upper, int divs)
{
	double length = upper-lower;
	double dl = length/divs;
	double sum=pow(lower,7)+pow(upper,7);
	double i;
	int go=1;
	for(i=lower+dl;i<upper-dl;i=i+dl)
	{
		if(go%2==0)
		{
			sum=sum+2*pow(i,7);
		}
		else
		{
			sum=sum+4*pow(i,7);
		}
		go=go+1;
	}
	sum=sum*dl/3;
	return sum;
}
double midpoint(int lower, int upper, int divs)
{
	double length = upper-lower;
	double dl = length/divs;
	double sum = 0;
	double i;
	for(i=lower;i<upper;i=i+dl)
	{
		sum=sum+pow(i,7);
	}
	sum=sum*dl;
	return sum;
}
