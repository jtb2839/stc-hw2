CC:= gcc

hw2_prog: hw2_prog.o hw2_routines.o
	$(CC) -o $@ $^ -lm
hw2_prog.o: hw2_prog.c hw2_routines.h
	$(CC) -c $<
hw2_routines.o: hw2_routines.c hw2_routines.h
	$(CC) -c $<
run: hw2_prog
	./hw2_prog
