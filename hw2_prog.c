//James Bell
#include "hw2_routines.h"
#include <stdio.h>
#include "math.h"
int main()
{
	int lower=0;
	int higher=1;
	int i=1;
	printf("Trapezoidal Rule:\n");
	for(i=1;i<6;i++)
	{
		int divs=pow(10,i);
		printf("n=%d,  percent error= %f\n",divs,(.125-trapezoid(lower,higher,divs))/.125*100);
	}
	printf("\nSimpson's Rule\n");
	for(i=1;i<6;i++)
	{
	        int divs=pow(10,i);
	        printf("n=%d,  percent error= %f\n",divs,(.125-simpsons(lower,higher,divs))/.125*100);
	} 
	printf("\nMidpoint Rule\n");
	for(i=1;i<6;i++)
	{
	        int divs=pow(10,i);
	        printf("n=%d,   percent error= %f\n",divs,(.125-midpoint(lower,higher,divs))/.125*100);
	} 
}
